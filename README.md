Ansible Raspberry Pi
====

* Edit `group_vars/all.yml`

* Configure user account and Wi-Fi
```
ansible-playbook -i raspberrypi.local, prepare.yml --user=pi --ask-pass
```

* Create and Edit inventory file
```
cp inventories/example.yml inventories/lan.yml
```

* Install docker
```
ansible-playbook -i inventories/lan.yml docker.yml
```
